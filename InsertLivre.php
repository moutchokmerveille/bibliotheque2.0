<?php
session_start();
if(empty($_SESSION['id'])){
  header('location: index.php');
}
require_once('Admin/connect.php');
$db = (new Database())->connect();

$filieres = $db->query('SELECT * FROM filiere ORDER BY ID_FILIERE DESC');
$specialites = $db->query('SELECT * FROM specialite ORDER BY ID_SPECIALITE DESC');

if (isset($_POST['save'])) {
  $nom_livre = $_POST['nom_livre'];
  $nom_auteur = $_POST['nom_auteur'];
  $idf = $_POST['idf'];
  $specialite = $_POST['specialite'];
  if (isset($_FILES['image']) and ($_FILES['file'])) {
    $img_name = $_FILES['image']['name'];
    $file_name = $_FILES['file']['name'];
    $tmp_name = $_FILES['image']['tmp_name'];
    $file_tmp_name = $_FILES['file']['tmp_name'];
    $img_explode = explode('.', $img_name);
    $file_explode = explode('.', $file_name);
    $img_ext = end($img_explode);
    $file_ext = end($file_explode);
    $extensions = ['jpeg', 'png', 'jpg'];
    $extensions_file = ['pdf'];
    if (in_array($img_ext, $extensions) == true and in_array($img_ext, $extensions) == true) {
      $time = time();

      $img = $time . $img_name;
      $file = $time . $file_name;
      if (move_uploaded_file($file_tmp_name, "./couveture/" . $file) and ($file_tmp_name)) {

        if (move_uploaded_file($tmp_name, "./couveture/" . $img) and ($tmp_name)) {
          $q = $db->prepare('INSERT INTO `livre`( `NOM_LIVRE`,`NOM_AUTEUR`,`ID_FILIERE`,`ID_SPECIALITE`,`Picture`,`File`) VALUES (?,?,?,?,?,?)');

          $q->execute(array($nom_livre, $nom_auteur, $idf, $specialite, $img,$file));
          $_SESSION['success'] = '<center>Le livre a ete enregistre avec succes!</center>';

          header("location:AdminLivres.php");
        }
      }
    }
  }
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Admin livre</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>

<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <!-- Navbar -->
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php include 'navAdmin.php'; ?>
    <!-- Main Sidebar Container -->
    <?php include 'AsideAdmin.php'; ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>LIVRES</h1>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <!-- /.card -->

              <div class="card">
                <div class="card-header">
                  <h4>Ajouter un nouveau livre</h4>
                </div>
                <div class="card-body">
                  <form method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nom du livre<span style="color: red">*</span></label>
                      <input type="text" required class="form-control" name="nom_livre" placeholder="Entrer le nom du livre">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nom de l'auteur<span style="color: red">*</span></label>
                      <input type="text" required class="form-control" name="nom_auteur" placeholder="Entrer le nom de l'auteur">
                    </div>
                    <div class="row">
                      <div class="form-group col-md-6">
                        <label for="exampleSelectBorder">Choisir la filiere<span style="color: red">*</span></label>
                        <select class="custom-select form-control-border" name="idf" id="exampleSelectBorder" required>
                          <?php
                          foreach ($filieres as $filiere) {
                          ?>
                            <option value="<?= $filiere['ID_FILIERE'] ?>"><?= $filiere['LIBELLES'] ?></option>
                          <?php
                          }
                          ?>
                        </select>
                      </div>
                      <div class="form-group col-md-6">
                        <label for="exampleSelectBorder">Choisir la specialite<span style="color: red">*</span></label>
                        <select class="custom-select form-control-border" name="specialite" id="exampleSelectBorder" required>
                          <?php
                          foreach ($specialites as $specialite) {
                          ?>
                            <option value="<?= $specialite['ID_SPECIALITE'] ?>"><?= $specialite['LIBELLE'] ?></option>
                          <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div><br>
                    <div class="row">
                      <div class="form-group col-md">
                        <label for="">Photo de couverture du livre<span style="color: red">*</span></label>
                        <input type="file" class="form-control" accept="image/x-png,image/gif,image/jpg,image/jpeg" name="image" />
                      </div>
                      <div class="form-group col-md">
                        <label for="">livre (Fichier)<span style="color: red">*</span></label>
                        <input type="file" class="form-control" accept=".pdf" name="file" />
                      </div>
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="save" class="btn btn-primary">Enregistrer</button>
                </div>
                </form>

                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="float-right d-none d-sm-block">
        <b>Version</b> 3.2.0
      </div>
      <strong>Copyright by Merveille 2022-2023 </strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- DataTables  & Plugins -->
  <script src="plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
  <script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
  <script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
  <script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
  <script src="plugins/jszip/jszip.min.js"></script>
  <script src="plugins/pdfmake/pdfmake.min.js"></script>
  <script src="plugins/pdfmake/vfs_fonts.js"></script>
  <script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
  <script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
  <script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
  <!-- Page specific script -->
  <script>
    $(function() {
      $("#example1").DataTable({
        "responsive": true,
        "lengthChange": false,
        "autoWidth": false,
        "buttons": ["copy", "pdf", "print"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
</body>

</html>