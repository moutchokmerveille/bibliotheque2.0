<?php
require_once('Admin/connect.php');
$db = (new Database())->connect();

if (isset($_GET['ID_LIVRE'])) {

    $id = $_GET['ID_LIVRE'];

    $q = $db->prepare('SELECT * FROM livre WHERE ID_lIVRE = ?');
    $q->execute(array($id));

    $filieres = $db->query('SELECT * FROM filiere ORDER BY ID_FILIERE DESC');
    $specialites = $db->query('SELECT * FROM specialite ORDER BY ID_SPECIALITE DESC');


    if (isset($_POST['modify'])) {

        $id = $_GET['ID_LIVRE'];

        $NOM_LIVRE = $_POST['NOM_LIVRE'];
        $NOM_AUTEUR = $_POST['NOM_AUTEUR'];

        $idf = $_POST['idf'];
        $specialite = $_POST['specialite'];

        $q = $db->prepare("UPDATE `livre` SET `NOM_LIVRE`=?,`NOM_AUTEUR`=?,`ID_FILIERE`=?,`ID_SPECIALITE`=? WHERE ID_LIVRE=?");
        $q->execute(array($NOM_LIVRE, $NOM_AUTEUR, $idf, $specialite, $id));

        header("location: AdminLivres.php");
    }
    foreach ($q as $update) {
?>

        <!DOCTYPE html>
        <html lang="en">

        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Admin livre</title>

            <!-- Google Font: Source Sans Pro -->
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
            <!-- Font Awesome -->
            <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
            <!-- DataTables -->
            <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
            <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
            <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
            <!-- Theme style -->
            <link rel="stylesheet" href="dist/css/adminlte.min.css">
        </head>

        <body class="hold-transition sidebar-mini">
            <div class="wrapper">
                <!-- Navbar -->
                <!-- /.navbar -->

                <!-- Main Sidebar Container -->
                <?php include 'navAdmin.php'; ?>
                <!-- Main Sidebar Container -->
                <?php include 'AsideAdmin.php'; ?>

                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h1>MODIFICATION DES LIVRES</h1>
                                </div>
                            </div>
                        </div><!-- /.container-fluid -->
                    </section>

                    <!-- Main content -->
                    <section class="content">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12">
                                    <!-- /.card -->

                                    <div class="card">
                                        <div class="card-header">
                                            <h4>modifier les livres</h4>
                                        </div>
                                        <div class="card-body">
                                            <form method="POST" enctype="multipart/form-data">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Nom du livre</label>
                                                    <input type="text" required class="form-control" value="<?= $update['NOM_LIVRE'] ?>" id="NOM_LIVRE" placeholder="Entrer le nom du livre" name="NOM_LIVRE" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Nom de l'auteur</label>
                                                    <input type="text" required class="form-control" value="<?= $update['NOM_AUTEUR'] ?>" id="NOM_AUTEUR" placeholder="Entrer le nom de l'auteur" name="NOM_AUTEUR" required>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md">
                                                        <label for="exampleSelectBorder">Choisir la filiere</label>
                                                        <select class="custom-select form-control-border" value="<?= $update['idf'] ?>" id="idf" placeholder="Entrer le nom de la filiere" name="idf" id="exampleSelectBorder" required>
                                                            <?php
                                                            foreach ($filieres as $filiere) {
                                                            ?>
                                                                <option value="<?= $update['ID_FILIERE'] ?>"><?= $filiere['LIBELLES'] ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md">
                                                        <label for="exampleSelectBorder">Choisir la specialite</label>
                                                        <select class="custom-select form-control-border" value="<?= $livres['specialite'] ?>" id="specialite" placeholder="Entrer le nom de la specialite" name="specialite" id="exampleSelectBorder" required>
                                                            <?php
                                                            foreach ($specialites as $specialite) {
                                                            ?>
                                                                <option value="<?= $specialite['ID_SPECIALITE'] ?>"><?= $specialite['LIBELLE'] ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div><br>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md">
                                                        <label for="">Photo de couverture du livre</label>
                                                        <input type="file" class="form-control" accept="image/x-png,image/gif,image/jpg,image/jpeg" name="image" />
                                                    </div>
                                                    <div class="form-group col-md">
                                                        <label for="">livre (Fichier)</label>
                                                        <input type="file" class="form-control" accept=".pdf" name="file" />
                                                    </div>
                                                </div>





                                        </div>
                                        <!-- /.card-body -->

                                        <div class="card-footer">
                                            <button type="submit" name="modify" class="btn btn-primary">modifier</button> <a href="AdminLivres.php" class="btn btn-success">Retour</a>
                                        </div>

                                        </form>

                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                <?php
            }
        }
                ?>
                <!-- /.container-fluid -->
                    </section>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->
                <footer class="main-footer">
                    <div class="float-right d-none d-sm-block">
                        <b>Version</b> 3.2.0
                    </div>
                    <strong>Copyright by Merveille 2022-2023 </strong> All rights reserved.
                </footer>

                <!-- Control Sidebar -->
                <aside class="control-sidebar control-sidebar-dark">
                    <!-- Control sidebar content goes here -->
                </aside>
                <!-- /.control-sidebar -->
            </div>
            <!-- ./wrapper -->

            <!-- jQuery -->
            <script src="plugins/jquery/jquery.min.js"></script>
            <!-- Bootstrap 4 -->
            <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
            <!-- DataTables  & Plugins -->
            <script src="plugins/datatables/jquery.dataTables.min.js"></script>
            <script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
            <script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
            <script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
            <script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
            <script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
            <script src="plugins/jszip/jszip.min.js"></script>
            <script src="plugins/pdfmake/pdfmake.min.js"></script>
            <script src="plugins/pdfmake/vfs_fonts.js"></script>
            <script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
            <script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
            <script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
            <!-- AdminLTE App -->
            <script src="dist/js/adminlte.min.js"></script>
            <!-- AdminLTE for demo purposes -->
            <script src="dist/js/demo.js"></script>
            <!-- Page specific script -->
            <script>
                $(function() {
                    $("#example1").DataTable({
                        "responsive": true,
                        "lengthChange": false,
                        "autoWidth": false,
                        "buttons": ["copy", "pdf", "print"]
                    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
                    $('#example2').DataTable({
                        "paging": true,
                        "lengthChange": false,
                        "searching": false,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false,
                        "responsive": true,
                    });
                });
            </script>
        </body>

        </html>