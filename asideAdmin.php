
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      
      
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img class="rounded-circle" src="images/inde.jpg" alt="User Image" style="height: 50px; width: 50px; ">
        </div>
        <div class="info">
          <a href="#" class="d-block"><h5>ISSTN DSCHANG</a></h5>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <?php 
          if (isset($_SESSION['id'])){
            ?>

          <li class="nav-item">
            <a href="AdminHome.php" class="nav-link">
              <i class="nav-icon fas fa-chart-line"></i>

              <p>
                Dashboard
              </p>
            </a>
          </li>
          
          <li class="nav-item">
            <a href="AdminLivres.php" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                livres
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="AdminEtudiant.php" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                etudiants
              </p>
            </a>
          </li>
          
            <li class="nav-item">
            <a href="AdminFiliere.php" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                filieres
              </p>
            </a>
          </li>
           
          <li class="nav-item">
            <a href="AdminSpecialite.php" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                specialites
              </p>
            </a>
          </li>
          <?php
          }
          ?>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>