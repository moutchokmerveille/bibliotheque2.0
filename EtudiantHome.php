<?php
session_start();
if(empty($_SESSION['idE'])){
  header('location: index.php');
}
require_once('Admin/connect.php');
$db = (new Database())->connect();

$filieres = $db->query('SELECT * FROM filiere ORDER BY ID_FILIERE DESC');
$specialites = $db->query('SELECT * FROM specialite ORDER BY ID_SPECIALITE DESC');
//$livres = $db->query('SELECT * FROM livre,specialite,filiere WHERE livre.ID_SPECIALITE = specialite.ID_SPECIALITE AND livre.ID_FILIERE = specialite.ID_FILIERE AND filiere.ID_FILIERE = specialite.ID_FILIERE ORDER BY ID_LIVRE DESC');
$q = $db->prepare('SELECT * FROM specialite');
//$livrestotals = $livres->rowCount();

$livresparpage = 4;
$livretotalreq = $db->query('SELECT ID_LIVRE FROM livre');
$livretotals = $livretotalreq->rowCount();
$pagestotales= ceil($livretotals/$livresparpage);
  if(isset($_GET['page']) and !empty($_GET['page'] and $_GET['page']>0 and $_GET['page'] <= $pagestotales))
  {
    $getpage = intval($_GET['page']);
    $pagecourante = $getpage;
  }else{
    $pagecourante = 1;
  }
$depart = ($pagecourante-1)*$livresparpage;
 $livres = $db->query('SELECT * FROM livre,specialite,filiere WHERE livre.ID_SPECIALITE = specialite.ID_SPECIALITE AND livre.ID_FILIERE = specialite.ID_FILIERE AND filiere.ID_FILIERE = specialite.ID_FILIERE ORDER BY ID_LIVRE DESC LIMIT '.$depart.','.$livresparpage.' ');
 $livrestotals = $livres->rowCount();

if (isset($_POST['sf'])) {
  $idf = $_POST['idf'];
  $livres = $db->query('SELECT * FROM livre,specialite,filiere WHERE livre.ID_FILIERE = '.$idf.' AND livre.ID_SPECIALITE = specialite.ID_SPECIALITE AND livre.ID_FILIERE = specialite.ID_FILIERE AND filiere.ID_FILIERE = specialite.ID_FILIERE ORDER BY ID_LIVRE DESC');
  $livrestotals = $livres->rowCount();
  $resultf = $db->query('SELECT LIBELLES FROM filiere WHERE ID_FILIERE = '.$idf.'');
  $rt = $resultf->rowCount();
  $l_info = $resultf->fetch();
}
if (isset($_POST['ss'])) {
  $ids = $_POST['ids'];
  $livres = $db->query('SELECT * FROM livre,specialite,filiere WHERE livre.ID_SPECIALITE = '.$ids.' AND livre.ID_SPECIALITE = specialite.ID_SPECIALITE AND livre.ID_FILIERE = specialite.ID_FILIERE AND filiere.ID_FILIERE = specialite.ID_FILIERE ORDER BY ID_LIVRE DESC');
  $livrestotals = $livres->rowCount();
  $resultf = $db->query('SELECT LIBELLE FROM specialite WHERE ID_SPECIALITE = '.$ids.'');
  $rts = $resultf->rowCount();
  $ls_info = $resultf->fetch();
}
if (isset($_POST['sn'])) {
  $noml = $_POST['noml'];
  $livres = $db->query('SELECT * FROM livre,specialite,filiere WHERE NOM_LIVRE LIKE "%'.$noml.'%" AND livre.ID_SPECIALITE = specialite.ID_SPECIALITE AND livre.ID_FILIERE = specialite.ID_FILIERE AND filiere.ID_FILIERE = specialite.ID_FILIERE ORDER BY ID_LIVRE DESC');
  $livrestotals = $livres->rowCount();
  
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>page de l'etudiant</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

  <link rel="stylesheet" href="assets/dist/css/glightbox.css" />
</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <!-- Preloader -->


    <!-- Navbar -->

    <!-- /.navbar -->
    <?php include 'navAdmin.php'; ?>
    <!-- Main Sidebar Container -->
    <?php include 'AsideAdmin.php'; ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">BIBLIOTHEQUE 2.0</h1>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <h3 style="float: left;">Nos Livres </h3>
          <h3 style="text-align: right;">Nombres de livres :<span class="badge badge-success"><?php echo $livrestotals ?></span></h3>
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Rechercher</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-3">
                  <label for="">Par Filiere</label>
                  <form action="" method="post">
                  <div class="input-group">
                      <select class="form-control" name="idf" id="exampleSelectBorder" required>
                      <option>Choisir la filiere</option>
                        <?php
                        foreach ($filieres as $filiere) {
                        ?>
                          <option value="<?= $filiere['ID_FILIERE'] ?>"><?= $filiere['LIBELLES'] ?></option>
                        <?php
                        }
                        ?>
                      </select>
                      <span class="input-group-append">
                        <button type="submit" name="sf" class="btn btn-success"><i class="fa fa-search"></i></button>
                      </span>
                    </div>
                  </form>
                </div>
                <div class="col-3">
                  <label for="">Par Specialite</label>
                  <form action="" method="post">
                  <div class="input-group">
                    <select class=" form-control" name="ids" id="exampleSelectBorder" required>
                      <option>Choisir la specialite</option>
                      <?php
                      foreach ($specialites as $specialite) {
                      ?>
                        <option value="<?= $specialite['ID_SPECIALITE'] ?>"><?= $specialite['LIBELLE'] ?></option>
                      <?php
                      }
                      ?>
                    </select>
                    <span class="input-group-append">
                      <button type="submit" name="ss" class="btn btn-success"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                  </form>
                </div>
                <div class="col-4">
                  <label for="">Par Nom du livre</label>
                  <form action="" method="post">
                  <div class="input-group">
                    <input type="text" class="form-control" name="noml" placeholder="Entrer le nom du livre">
                    <span class="input-group-append">
                      <button type="submit" name="sn" class="btn btn-success"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                  </form>
                </div>
                <div class="col-2">
                  <label for="">Tous les livres</label><br>
                  <a href="EtudiantHome.php" class="btn btn-success"><i class="fa fa-search"></i></a>
                </div>
              </div>
            </div>
          </div>
          <!-- /.card-body -->
          <?php 
          if(isset($noml)){
            ?>
            <h3 >Resultat recherche pour : <span class="text-primary"><?= $noml?></span></h3>
            <?php
          }
          ?>
          <?php 
          if(isset($rt) and $rt > 0){
            ?>
            <h3 >Resultat pour la filiere : <span class="text-primary"><?= $l_info['LIBELLES']?></span></h3>
            <?php
          }
          ?>
          <?php 
          if(isset($rts) and $rts > 0){
            ?>
            <h3 >Resultat pour la specialite : <span class="text-primary"><?= $ls_info['LIBELLE']?></span></h3>
            <?php
          }
          ?>
          <?php 
          if($livrestotals < 1){
            ?>
            <h3 class="text-warning">Aucun livre trouve!!!</h3>
            <?php
          }
          ?>
        </div>
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <?php
          foreach ($livres as $livre) {
          ?>
            <div class="TOTO">
              <div class="col-3">
                <div class="card" style="width: 16rem;">
                  <img src="./couveture/<?= $livre['Picture'] ?>" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h3 class="card-title"><strong><?= $livre['NOM_LIVRE'] ?></strong></h3>
                    <p class="card-text"></p>
                    <ol class="list-group ">
                      <li class="list-group-item">Auteur: <?php echo $livre['NOM_AUTEUR'] ?></li>
                      <li class="list-group-item">Filiere: <?= $livre['LIBELLES'] ?></li>
                      <li class="list-group-item">Specialite: <?= $livre['LIBELLE'] ?></li>
                    </ol>
                  </div>
                  <div class="card-footer">
                    <!-- <iframe src="lucrece.pdf" width="100%" style="height:100%"></iframe> -->
                    <a href="./couveture/<?= $livre['File'] ?>" class="glightbox btn btn-outline-primary">Voir</a>
                    <a href="./couveture/<?= $livre['File'] ?>" aria-hidden="true" download="./couveture/<?= $livre['File'] ?>" class="btn btn-outline-primary">Telecharger</a>
                    <br>

                  </div>
                </div>
              </div>
            </div>
          <?php
          }
          ?>
        
        </div>
        <?php
      for($i=1;$i<=$pagestotales;$i++)
      {    
        if($i==$pagecourante)
        {
              echo  '<span class="btn btn-primary mr-2 mb-3">'.$i.'</span>';
        }
        else{
           echo '<a href="EtudiantHome.php?page='.$i.'" class="btn btn-outline-primary mr-2 mb-3">'.$i.'</a>';
        }
          
           
      }
      ?>

        <!-- /.row -->
        <!-- Main row -->

        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- ChartJS -->
  <script src="plugins/chart.js/Chart.min.js"></script>
  <!-- Sparkline -->
  <script src="plugins/sparklines/sparkline.js"></script>
  <!-- JQVMap -->
  <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
  <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="plugins/moment/moment.min.js"></script>
  <script src="plugins/daterangepicker/daterangepicker.js"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <!-- Summernote -->
  <script src="plugins/summernote/summernote-bs4.min.js"></script>
  <!-- overlayScrollbars -->
  <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="dist/js/pages/dashboard.js"></script>
  <script src="assets/dist/js/glightbox.js"></script>

  <script>
    var lightbox = GLightbox();
    lightbox.on('open', (target) => {
      console.log('lightbox opened');
    });
    var lightboxDescription = GLightbox({
      selector: '.glightbox2'
    });
    var lightboxVideo = GLightbox({
      selector: '.glightbox3'
    });
    lightboxVideo.on('slide_changed', ({
      prev,
      current
    }) => {
      console.log('Prev slide', prev);
      console.log('Current slide', current);

      const {
        slideIndex,
        slideNode,
        slideConfig,
        player
      } = current;

      if (player) {
        if (!player.ready) {
          // If player is not ready
          player.on('ready', (event) => {
            // Do something when video is ready
          });
        }

        player.on('play', (event) => {
          console.log('Started play');
        });

        player.on('volumechange', (event) => {
          console.log('Volume change');
        });

        player.on('ended', (event) => {
          console.log('Video ended');
        });
      }
    });

    var lightboxInlineIframe = GLightbox({
      selector: '.glightbox4'
    });
  </script>
</body>

</html>