<?php 
    session_start();
    require_once ('Admin/connect.php');
    $emailError = $passwordError = "";
    if(isset($_POST['submit'])){
        if(!empty($_POST['email']) && !empty($_POST['password'])){

            $email= htmlspecialchars($_POST['email']);
            $password= $_POST['password'];

           
                if (preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#",$_POST['email'])) {
                        $db = Database::connect();
                        $statement = $db->prepare("INSERT INTO administrateur (email,password) VALUES(?, ?)");
                        $statement->execute(array($email,$password));
            
                        $recupUser = $db->prepare("SELECT * FROM administrateur WHERE email = ? AND password = ?");
                        $recupUser->execute(array($email,$password));
                        if ($recupUser->rowCount() > 0) {
                           
                            $_SESSION['email'] = $email;
                            $_SESSION['password'] = $password;
                            $_SESSION['id'] = $recupUser->fetch();
                            header('Location: index.php');
                        }
                }else {
                    $emailError = "L'email n'est pas valide";
                }
            
           
        }else{
            $error = "Veuiller remplir tous les champs...";
        }
    }


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Inscription</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">
    <script
    src="https://code.jquery.com/jquery-3.6.1.js"
    integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI="
    crossorigin="anonymous"></script>

</head>

<body class="animsition">
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">  
                    <?php
                    if (isset($error)) { ?>
                        <div class="alert alert-danger" role="alert">
                            <?php echo $error; ?>
                        </div> <?php } ?>                      
                        <div class="login-logo">
                            <a href="#">
                            <img src="images/inde.jpg" style="width: 120px;" alt="Logo_ISSTN">
                            </a>
                        </div>
                        <div class="login-form">
                            <form action="" method="POST">
                                
                                <div class="form-group">
                                    <label>Adresse Email</label>
                                    <input class="au-input au-input--full" type="text" name="email" placeholder="Email..." autocomplete="off">
                                    <span class="help-inline"><?php echo $emailError;?></span>
                                </div>
                                <div class="form-group">
                                    <label>Mot De Passe</label>
                                    <input class="au-input au-input--full" type="password" name="password" placeholder="Mot de passe..." autocomplete="off">
                                    <span class="help-inline"><?php echo $passwordError;?></span>
                                </div><br>
                                <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit" name="submit">S'insrire</button>
                            </form>
                            <div class="register-link">
                                <p>
                                    J'ai d&eacute;ja un compte?
                                    <a href="index.php"><u>Se connecter</u></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>
<!-- end document-->